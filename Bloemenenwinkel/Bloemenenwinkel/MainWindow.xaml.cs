﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Bloemenenwinkel
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void imgRozen_MouseEnter(object sender, MouseEventArgs e)
        {
            rctRed.Visibility = Visibility.Visible;
        }

        private void imgLavender_MouseEnter(object sender, MouseEventArgs e)
        {
            rctLavendel.Visibility = Visibility.Visible;
        }

        private void imgGeel_MouseEnter(object sender, MouseEventArgs e)
        {
            rctGeek.Visibility = Visibility.Visible;
        }

        private void imgOrange_MouseEnter(object sender, MouseEventArgs e)
        {
            rctOrgange.Visibility = Visibility.Visible;
        }

        private void imgRozen_MouseLeave(object sender, MouseEventArgs e)
        {
            rctRed.Visibility = Visibility.Hidden;
        }

        private void imgLavender_MouseLeave(object sender, MouseEventArgs e)
        {
            rctLavendel.Visibility = Visibility.Hidden;
        }

        private void imgGeel_MouseLeave(object sender, MouseEventArgs e)
        {
            rctGeek.Visibility = Visibility.Hidden;
        }

        private void imgOrange_MouseLeave(object sender, MouseEventArgs e)
        {
            rctOrgange.Visibility = Visibility.Hidden;
        }
    }
}
